#!/usr/bin/env python
# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE
import sys
import json
import Queue
import threading
import time
import fcntl
import os

class CommandLineThread(threading.Thread):
	def __init__(self, queue, type, stdinQ):
		threading.Thread.__init__(self)
		self.queue = queue
		self.type = type
		self.stdinQ = stdinQ
		self.queueLock = threading.Lock()
		
	def nonBlockRead(self, output):
		fd = output.fileno()
		fl = fcntl.fcntl(fd, fcntl.F_GETFL)
		fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
		try:
			return output.read()
		except:
			return ''
       	 
	def run(self):
		self.p = Popen(["node", "Agent.js", self.type], stdout=PIPE, stdin = PIPE, bufsize=1)
		while True:
			stdout = self.nonBlockRead(self.p.stdout)
			lines = stdout.splitlines()
			for line in lines:
				self.queue.put(json.loads(line))
			while not self.stdinQ.empty():
				val = self.stdinQ.get()
				self.stdinQ.task_done()
				jsonString = json.dumps({"type": "message", "to": val[1], "msg":val[0]})
				self.p.stdin.write(jsonString + "\n")
			time.sleep(0.001)
		
class Agent:

	def __init__(self, type):
		self.internal_types = {}
		self.queue = Queue.Queue()
		self.stdinQ = Queue.Queue()
		self.t = CommandLineThread(self.queue, type, self.stdinQ)
		self.t.daemon = True
		self.t.start()
		self.connected = False
		
	def getMessages(self):
		messages = list()
		while not self.queue.empty():
			val = self.queue.get()
			messages.append(str(val))
			self.receiveMessage(str(val))
			self.queue.task_done()
			self.connected = True
		
	def sendMessage(self, msg, to=[]):
		self.stdinQ.put((msg,to))	
		
	# Put annotation
	def receiveMessage(self, message):
		message = message.replace("u\'", '"').replace("\'",'"')
		message = json.loads(message)
		if message["type"] == "connectedUsers":
			self.connectedUsers = message["data"]
		elif message["type"] == 'newuser':
			self.connectedUsers.append(message["data"])
		elif message["type"] == 'login_data':
			self.username = message["username"]
			self.password = message["password"]
		elif message["type"] == "message":
			self.processMessage(message)
		else:
			print "Unknown message:", message
			
	def processMessage(self, msg):
		pass