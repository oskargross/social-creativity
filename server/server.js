//Include relevant packages
function makepass()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 10; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function authenticate(username, password){
	i = usernames.indexOf(username);
	if(i > -1 && passwords[i] == password){
		return true;
	}
	else{
		return false;
	}	
}

function getSocket(username){
	i = usernames.indexOf(username);
	if(i > -1){
		return socketIds[i];
	}
	else{
		return null;
	}	
}

var fs = require('fs')
    , http = require('http')
    , socketio = require('socket.io');
    
var md5 = require('crypto-js/md5');

var TYPE = {
	OBSERVER : "observer", 
	ACTOR : "actor"
};

var usernames = [];
var passwords = [];
var socketIds = [];

//Create a server to port 5000
var server = http.createServer(function(req, res) {}).listen(5000, function() {
    console.log('Listening at: http://localhost:5000');
});
 
io = socketio.listen(server)
//On connection event
io.on('connection', function (socket) {

	// This is for subscribing to room	
    socket.on('requestid', function() { 
    	index = 0;
    	while(index > -1){
        	username = md5(new Date().getTime().toString()).toString();
        	index = usernames.indexOf(username);
        }
        password = makepass();
        
        io.sockets.in(TYPE.OBSERVER).emit("connectedUsers", usernames);
        socket.emit("connectedUsers", usernames);
        
        usernames.push(username);
        passwords.push(password);
        socketIds.push(socket);
        socket.emit("login_data", {username: username, password: password});
        
        socket.emit("responseid", {username: username, password: password, socketid: socket.id, 'version': 1});
        socket.broadcast.emit("newuser", username);
    })
    
	// This is for subscribing to room	
    socket.on('subscribe', function(room) { 
        socket.join(room);
    	console.log('Agent joins room: ', room);
        
    })
	// This is for leaving a room
    socket.on('unsubscribe', function(room) {  
        console.log('leaving room', room);
        socket.leave(room); 
    })
    
    socket.on("message", function(data){
        io.sockets.in(TYPE.OBSERVER).emit("message", data);
    	if(authenticate(data.username, data.password)){
    		if(data.to.length == 0){
				socket.broadcast.emit("message", {from: data.username, msg: data.msg, 'version': 1})   		
    		}
    		else{
				for(i = 0; i < data.to.length; i++){
					getSocket(data.to[i]).emit("message", {from: data.username, msg: data.msg, 'version': 1});
				}
    		}
    	}
    	else{
    		socket.emit("megaError", 1);
    	}
    });
});