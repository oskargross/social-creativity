#!/usr/bin/env python
# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE
import sys
import json
import Agent as a
import random
import time

class AgentImpl(a.Agent):

	def __init__(self, type):
		a.Agent.__init__(self, type)
		##
		## Here we define the internal protocol types
		## and their processing functions
	
		self.internal_types = {'getFunction': self.getFunction,
							   'sendFunction': self.sendFunction,
							  }
		id = random.randint(0,10)
		greeted = False
		counter = 1
		start = time.time()
		while True:
			new_messages = self.getMessages()
			if len(new_messages) > 0:
				for n in new_messages:
					self.receiveMessage(n)
			if self.connected == True:
				w = self.createWord()
				self.sendMessage(w)
			time.sleep(0.001)
			
	def processMessage(self, msg):
		print "I received message from", msg["from"], msg["msg"]
		
	def createWord(self):
		alpha = "abcdefghijklmnopqrstu"
		w = ""
		for i in range(0,random.randint(0,50)):
			w += alpha[random.randint(0,len(alpha)-1)]
		return w
		
	def getFunction(self):
		pass
		
	def sendFunction(self):
		pass
			
		
if __name__ == "__main__":		
	a = AgentImpl("actor")