function Agent(type){
	var socket = require('socket.io-client')('http://localhost:5000');

	var username = null;
	var password = null;
	
	var type = type;
	
	var self = this;
	
	self.sendMessage = function(to, message){
		if(to == null){
			socket.emit("message", {to: [], username: username, password: password, msg: message});
		}
		else{
			if(Array.isArray(to)){
				socket.emit("message", {to: to, username: username, password: password, msg: message});		
			}
			else{
				socket.emit("message", {to: [to], username: username, password: password, msg: message});		
			}
		}
	};

	socket.on('connect', function(){
		if(type == Agent.TYPE.OBSERVER && username == null){
			socket.emit("subscribe", Agent.TYPE.OBSERVER);
			username = "obs";
			console.log("Connected as observer.");
		}
		else if(type == Agent.TYPE.ACTOR && username == null){
			socket.emit("subscribe", Agent.TYPE.ACTOR);
			console.log("Connected as actor.");	
			socket.emit("requestid");
		}
	});

	socket.on("message", function(data){
		console.log('Incoming message:', data);
	});

	socket.on("responseid", function(data){
		username = data.username;
		password = data.password;
		console.log("Succesfully received login data.");
	});

	socket.on("newuser", function(to){
		console.log("Greeting new user!");
		self.sendMessage(to, "AGAOIHGAOHAGOIHASOIHASIOASHG");	
	});

	socket.on("connectedUsers", function(data){
		otherUsers = data;
		console.log('Other users:', data);
	});

	socket.on("megaError", function(data){
		console.log('OH NO:', data);
	});

}

Agent.TYPE = {
	OBSERVER : "observer", 
	ACTOR : "actor"
};

a = new Agent(Agent.TYPE.ACTOR)