import Agent as agent
import random
import time
from Agent import Feedback

class VowelAgent(agent.Agent):
	"""
    VowelAgent implements a sample functional agent.
    """
	def __init__(self, name, genVow):
		# Anything you want to initialize
		self.vowelWeight = random.random()
		self.consonantWeight = random.random()
		self.generateVowel = genVow
		agent.Agent.__init__(self, name)
		
	def lifeCycle(self):
		"""
		The function which is repeatedly started and defines the behaviour
		of the agent in the context of adaption, scoring and generating.
		"""
		r = random.random()
		self.getMessages()
		if r < 0.33:
			self.generate()
		if r > 0.33 and r < 0.66:
			## Gives the lists of lists, where each sublist is, ordered by timestamp desc:
			## [Word, Word], see documentation
			unratedwords = self.getUnscoredWords()
			if len(unratedwords) > 0:
				scr = self.score(unratedwords[0].word)
				framing = unicode("This is not a nice word")
				self.sendFeedback(unratedwords[0].agent_id, scr, framing, unratedwords[0].timestamp, 
				unratedwords[0].explanation, wordtext=unratedwords[0].word)
				self.unscored = self.unscored[1:]
		elif r > 0.66:
			## The result is the following:
			## [Feedback, Feedback], see documentation
			feedback = self.getAllFeedback()
			self.adapt(feedback)
			
		time.sleep(0.001)
		
	def score(self, word):
		"""
		score implements a sample function which gives a score to a word. In this case
		it the score is calculated such, that the word has a desired fraction of
		consonants and vowels.
		"""
		vowels = 0.0
		consonants = 0.0
		vowelstr = "aeiou"
		for letter in word:
			if letter in vowelstr:
				vowels += 1.0
			else:
				consonants += 1.0
		vowels = vowels/(vowels+consonants)
		scr = 1-abs(self.generateVowel - vowels)
		return scr
		
	def generate(self):
		"""
		generate implements a sample function which generates a word with random
		length. The system makes a distinction between consonants and vowels and generates
		words by using a self.generateVowel variable, which adapts to the feedback.
		"""
		strlen = random.randint(4,20)
		word = ""
		vowels = "aeiou"
		consonants = "bcdfghjklmnpqrstvxyz"
		for i in range(strlen):
			r = random.random()
			if r < self.generateVowel:
				word += vowels[random.randint(0,len(vowels)-1)]
			else:
				word += consonants[random.randint(0,len(consonants)-1)]
		explanation = "I find the attribute phrase vowels to be as high as I prefer"
		## If the word is ready, propose it to the server
		self.propose(unicode(word), unicode(explanation))
		
	def adapt(self, feedback):
		"""
		adapt implements a sample function which changes the self.generateVowel
		according to the feedback of other agents.
		"""
		vowelsCount = 0.0
		consCount = 0.0
		vowels = "aeiou"
		consonants = "bcdfghjklmnpqrstvxyz"
		for f in feedback:
			frac = f[5]
			self.generateVowel += (self.generateVowel-frac)*0.01
		self.generateVowel = max(min(1,self.generateVowel),0)
		print "Adapting to generate vowels with frequency " + str(self.generateVowel)