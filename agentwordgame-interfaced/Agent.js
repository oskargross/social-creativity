function debug(str1){
	var fs = require('fs');
	fs.appendFile("log.txt", str1)
}

function Agent(type){
	var socket = require('socket.io-client')('http://localhost:5000');

	var username = null;
	var password = null;
	var socketid = null;
	
	var type = type;
	
	var self = this;
	
	self.sendMessage = function(to, message){
		if(to == null){
			socket.emit("message", {to: [], username: username, password: password, msg: message, 'version': 1});
		}
		else{
			if(Array.isArray(to)){
				socket.emit("message", {to: to, username: username, password: password, msg: message, 'version': 1});		
			}
			else{
				socket.emit("message", {to: [to], username: username, password: password, msg: message, 'version': 1});		
			}
		}
	};

	socket.on('connect', function(){
		if(type == Agent.TYPE.OBSERVER && username == null){
			socket.emit("subscribe", Agent.TYPE.OBSERVER);
			username = "obs";
		}
		else if(type == Agent.TYPE.ACTOR && username == null){
			socket.emit("subscribe", Agent.TYPE.ACTOR);
			socket.emit("requestid");
		}
	});

	socket.on("message", function(data){
		data["type"] = "message"
		console.log(JSON.stringify(data));
	});

	socket.on("responseid", function(data){
		username = data.username;
		password = data.password;
		socketid = data.socketid;
		console.log(JSON.stringify({username: username, password: password, type: "login_data", 'version': 1}));
	});

	socket.on("newuser", function(who){
		console.log(JSON.stringify({type: "newuser", data: who}));
	});

	socket.on("connectedUsers", function(data){
		data = {"type": "connectedUsers", "data": data}
		console.log(JSON.stringify(data));
	});

	socket.on("megaError", function(data){
	});
	
	var readline = require('readline');
	var rl = readline.createInterface({
  		input: process.stdin,
  		output: new require('stream').Writable()
	});
	
	rl.on('line', function (cmd) {
	  	data = JSON.parse(cmd);
	  	self.sendMessage(data.to, data.msg);
	});

}

Agent.TYPE = {
	OBSERVER : "observer", 
	ACTOR : "actor"
};

type = -1

process.argv.forEach(function (val, index, array) {
	if(index == 2){
		if(val == "observer"){
			type = Agent.TYPE.OBSERVER;
		}
		else if(val == "actor"){
			type = Agent.TYPE.ACTOR;
		}
	}
});

a = new Agent(type)